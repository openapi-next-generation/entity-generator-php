<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Result;

use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\PrettyPrinter\Standard;
use PhpParser\PrettyPrinterAbstract;

class Collector
{
    protected array $generatedEntities = [];
    protected array $entities = [];
    protected array $collections = [];

    protected PrettyPrinterAbstract $codePrinter;


    public function __construct(PrettyPrinterAbstract $codePrinter = null)
    {
        $this->codePrinter = $codePrinter ?? new Standard([
            'shortArraySyntax' => true
        ]);
    }

    public function getGeneratedEntities(): array
    {
        return $this->generatedEntities;
    }

    public function addGeneratedEntity(Namespace_ $generatedEntity): void
    {
        $this->generatedEntities[$this->findClassName($generatedEntity)] = "<?php\n\n" . $this->codePrinter->prettyPrint([$generatedEntity]);
    }

    public function getEntities(): array
    {
        return $this->entities;
    }

    public function addEntity(Namespace_ $entity): void
    {
        $this->entities[$this->findClassName($entity)] = "<?php\n\n" . $this->codePrinter->prettyPrint([$entity]);
    }

    public function getCollections(): array
    {
        return $this->collections;
    }

    public function addCollection(Namespace_ $collection): void
    {
        $this->collections[$this->findClassName($collection)] = "<?php\n\n" . $this->codePrinter->prettyPrint([$collection]);
    }

    /**
     * Find the class in namespace and return class name
     */
    protected function findClassName(Namespace_ $namespace): string
    {
        foreach ($namespace->stmts as $statement) {
            if ($statement instanceof Class_) {
                return $statement->name;
            }
        }
    }
}