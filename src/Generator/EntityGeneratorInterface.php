<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator;

use OpenapiNextGeneration\EntityGeneratorPhp\Result\Collector;

interface EntityGeneratorInterface
{
    const NAMESPACE_GENERATED_ENTITIES = 'GeneratedEntities';
    const NAMESPACE_ENTITIES = 'Entities';
    const NAMESPACE_COLLECTIONS = 'Collections';

    /**
     * Process pattern definitions and create an entity structure based on processed patterns
     *
     * @param array $patterns
     * @param string $targetNamespace
     * @return null
     */
    public function processPatterns(array $patterns, string $targetNamespace);

    /**
     * Retrieve result collector containing all generated classes
     *
     * @return Collector
     */
    public function getResultCollector(): Collector;
}