<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator;

use PhpParser\BuilderFactory;

class UseCollector
{
    protected ?string $targetNamespace;
    protected array $uses = [];


    public function __construct(string $targetNamespace = null)
    {
        $this->targetNamespace = $targetNamespace;
    }

    /**
     * Return the class base name and register the class for uses
     */
    public function useClass(string $className): string
    {
        $baseNameSeparator = strrpos($className, '\\');
        $baseName = substr($className, $baseNameSeparator + 1);
        $namespace = substr($className, 0, $baseNameSeparator);
        if (isset($this->uses[$baseName])) {
            if ($this->uses[$baseName] !== $className) {
                throw new \Exception('Using two classes with same basename: ' . $this->uses[$baseName] . ' and ' . $className);
            }
        } elseif ($namespace !== $this->targetNamespace) {
            $this->uses[$baseName] = $className;
        }

        return $baseName;
    }

    /**
     * Build use statements for PhpParser
     */
    public function buildUseStatements(): array
    {
        sort($this->uses);
        $builder = new BuilderFactory();

        $statements = [];
        foreach ($this->uses as $class) {
            $statements[] = $builder->use($class);
        }

        return $statements;
    }
}