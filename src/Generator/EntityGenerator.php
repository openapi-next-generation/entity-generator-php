<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator;

use OpenapiNextGeneration\EntityGeneratorPhp\Config\GenerationConfig;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\AbstractGeneratedEntity;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\Collection;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\Entity;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity;
use OpenapiNextGeneration\EntityGeneratorPhp\Result\Collector;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\PropertyPattern;

class EntityGenerator implements EntityGeneratorInterface
{
    protected $config;
    protected $collector;


    public function __construct(GenerationConfig $config = null)
    {
        $this->config = $config ?? new GenerationConfig();
        $this->collector = new Collector();
    }

    /**
     * Process pattern definitions and create an entity structure based on processed patterns
     */
    public function processPatterns(array $patterns, string $targetNamespace): void
    {
        $abstractGeneratedEntity = new AbstractGeneratedEntity($this->config, $targetNamespace);
        $this->collector->addGeneratedEntity($abstractGeneratedEntity->build());

        /* @var $pattern PropertyPattern */
        foreach ($patterns as $pattern) {
            if ($pattern instanceof EntityPattern) {
                $generatedEntity = new GeneratedEntity($this->config, $targetNamespace, $this->collector);
                $this->collector->addGeneratedEntity($generatedEntity->build($pattern));

                $entity = new Entity($this->config, $targetNamespace);
                $this->collector->addEntity($entity->build($pattern));
            } elseif ($pattern instanceof ArrayPattern) {
                $collection = new Collection($this->config, $targetNamespace);
                $this->collector->addCollection($collection->build($pattern));
            }
        }
    }

    /**
     * Retrieve result collector containing all generated classes
     */
    public function getResultCollector(): Collector
    {
        return $this->collector;
    }
}