<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity;

use OpenapiNextGeneration\EntityGeneratorPhp\Config\GenerationConfig;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\AbstractPattern;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\Collection;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\UseCollector;
use OpenapiNextGeneration\EntityGeneratorPhp\Result\Collector;
use OpenapiNextGeneration\GenerationHelperPhp\TypeMapper;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AbstractContainerPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AssocPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\PropertyPattern;
use SimpleCollection\ArrayCollection;
use SimpleCollection\AssocCollection;
use SimpleCollection\Entity\EntityArrayCollection;

class Property extends AbstractPattern
{
    protected Collector $collector;
    protected PropertyPattern $pattern;
    protected string $type;


    public function __construct(
        GenerationConfig $config,
        string $targetNamespace,
        UseCollector $useCollector,
        Collector $collector,
        PropertyPattern $pattern
    )
    {
        parent::__construct($config, $targetNamespace, $useCollector);
        $this->collector = $collector;
        $this->pattern = $pattern;
        $this->type = $this->determineType($pattern);
    }

    public function getPattern(): PropertyPattern
    {
        return $this->pattern;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function build(): \PhpParser\Builder\Property
    {
        $property = $this->builder->property($this->pattern->getLowerCamelCaseName());
        $property->makePublic();
        $property->setType($this->getPhpType());

        return $property;
    }

    public function getPhpType(): string
    {
        $typePrefix = '';
        if (
            !$this->pattern instanceof AbstractContainerPattern
            && $this->pattern->isNullable()
        ) {
            $typePrefix = '?';
        }
        return $typePrefix . $this->type;
    }

    protected function determineType(PropertyPattern $pattern): string
    {
        if ($pattern instanceof EntityPattern) {
            $type = $this->useCollector->useClass(
                $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_ENTITIES . '\\' . $pattern->getClassName()
            );
        } elseif ($pattern instanceof AbstractContainerPattern) {
            $type = $this->useCollector->useClass(
                $this->determineCollectionType($pattern)
            );
        } else {
            $type = TypeMapper::mapType($pattern->getType());
        }

        return $type;
    }

    /**
     * Decides which collection class will be used for property
     */
    protected function determineCollectionType(AbstractContainerPattern $pattern): string
    {
        $contentPattern = $pattern->getContentProperty();
        if (
            $contentPattern instanceof EntityPattern ||
            $contentPattern instanceof AbstractContainerPattern
        ) {
            if ($pattern instanceof ArrayPattern) {
                $type = EntityArrayCollection::class;
            } elseif ($pattern instanceof AssocPattern) {
                $type = AssocCollection::class;
            }
            if ($contentPattern instanceof EntityPattern && $this->config->isAddCustomCollections()) {
                $collection = new Collection($this->config, $this->targetNamespace);
                $this->collector->addCollection($collection->build($pattern));
                $type = $collection->getClass();
            }
        } else {
            if ($pattern instanceof ArrayPattern) {
                $type = ArrayCollection::class;
            } elseif ($pattern instanceof AssocPattern) {
                $type = AssocCollection::class;
            }
        }
        return $type;
    }
}