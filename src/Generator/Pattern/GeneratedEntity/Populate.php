<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity;

use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\AbstractPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AbstractContainerPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AssocPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\PropertyPattern;
use PhpParser\Builder\Method;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\BinaryOp\Identical;
use PhpParser\Node\Expr\BinaryOp\NotIdentical;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\Foreach_;
use PhpParser\Node\Stmt\If_;
use SimpleCollection\ArrayCollection;
use SimpleCollection\AssocCollection;

class Populate extends AbstractPattern
{
    /**
     * Create a method that populates the entity
     */
    public function build(array $properties): Method
    {
        $populate = $this->builder->method('populate');
        $populate->makePublic();
        $populate->addParam($this->builder->param('data')->setType('array'));
        $populate->addStmts($this->createPopulateBody($properties));

        return $populate;
    }

    /**
     * Creates the body of the populate method
     */
    protected function createPopulateBody(array $properties): array
    {
        $populations = [];
        foreach ($properties as $property) {
            foreach ($this->createPopulationCall($property) as $statement) {
                $populations[] = $statement;
            }
        }

        return $populations;
    }

    /**
     * Creates the code to populate one property of the entity
     */
    protected function createPopulationCall(Property $property): array
    {
        $pattern = $property->getPattern();

        if ($pattern instanceof AbstractContainerPattern) {
            $populationCalls = $this->createContainerPatternPopulateCall($property, $this->createDataAccessCall($pattern));
        } elseif ($pattern instanceof EntityPattern) {
            $populationCalls = $this->createClassSetterCall($property);
        } else {
            if ($pattern->isNullable() || $pattern->getDefault() !== null) {
                $dataAccess = $this->createDataAccessCall($pattern);
            } else {
                $dataAccess = $this->createDataAccessCall($pattern, false);
            }
            $populationCalls = [$this->createSetCall($pattern, $dataAccess)];
        }

        return $populationCalls;
    }

    protected function createDataAccessCall(PropertyPattern $pattern, bool $coalesceCheck = true): Expr
    {
        if ($pattern->getDefault() !== null) {
            $defaultValue = $pattern->getDefault();
        } elseif (
            $pattern instanceof AbstractContainerPattern
            || $pattern instanceof EntityPattern && $pattern->isRequired() && !$pattern->isNullable()
        ) {
            $defaultValue = [];
        } else {
            $defaultValue = null;
        }

        $dataAccess = new Expr\ArrayDimFetch(
            $this->builder->var('data'),
            $this->builder->val($pattern->getName())
        );
        if ($coalesceCheck) {
            $dataAccess = new Expr\BinaryOp\Coalesce(
                $dataAccess,
                $this->builder->val($defaultValue)
            );
        }
        return $dataAccess;
    }

    protected function createSetCall(
        PropertyPattern $pattern,
        Expr $dataAccess
    ): Stmt
    {
        if ($pattern->isNullable() || $pattern->getDefault() !== null) {
            return new Expression(
                new Expr\Assign(
                    $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName()),
                    $dataAccess
                )
            );
        } else {
            return new If_(
                $this->builder->funcCall('isset', [$dataAccess]),
                [
                    'stmts' => [
                        new Expression(
                            new Expr\Assign(
                                $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName()),
                                $dataAccess
                            )
                        )
                    ]
                ]
            );
        }
    }

    protected function createClassSetterCall(Property $property): array
    {
        $pattern = $property->getPattern();

        if ($property->getPattern()->isNullable()) {
            $dataAccess = $this->createDataAccessCall($pattern, false);
            return [$this->createSetCall(
                $property->getPattern(),
                new Expr\Ternary(
                    $this->builder->funcCall('isset', [$dataAccess]),
                    $this->builder->new(
                        $property->getType(),
                        [$dataAccess]
                    ),
                    $this->builder->val(null)
                )
            )];
        } else {
            $dataAccess = $this->createDataAccessCall($pattern);
            if ($pattern->isRequired() && !$pattern->isNullable()) {
                return [
                    new Expr\Assign(
                        $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName()),
                        $this->builder->new(
                            $property->getType(),
                            [$dataAccess]
                        )
                    )
                ];
            } else {
                $setValue = $this->builder->var('setValue');
                return [
                    new Expr\Assign($setValue, $dataAccess),
                    new If_(
                        new NotIdentical($setValue, $this->builder->val(null)),
                        [
                            'stmts' => [
                                new Expression(
                                    new Expr\Assign(
                                        $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName()),
                                        $this->builder->new(
                                            $property->getType(),
                                            [$setValue]
                                        )
                                    )
                                )
                            ]
                        ]
                    )
                ];
            }
        }
    }

    protected function createContainerPatternPopulateCall(
        Property $property,
        Expr $dataAccess
    ): array
    {
        $statements = [];

        /* @var AbstractContainerPattern $pattern */
        $pattern = $property->getPattern();
        $statements[] = new Expression(
            new Expr\Assign(
                $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName()),
                $this->builder->new($property->getType())
            )
        );

        $contentProperty = $pattern->getContentProperty();
        $setValue = $item = $this->builder->var('item');
        if ($contentProperty instanceof EntityPattern) {
            $setValue = $this->builder->new(
                $this->useCollector->useClass(
                    $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_ENTITIES . '\\' . $contentProperty->getClassName()
                ),
                [$setValue]
            );
        } elseif ($contentProperty instanceof ArrayPattern) {
            $setValue = $this->builder->new(
                $this->useCollector->useClass(
                    ArrayCollection::class
                ),
                [$setValue]
            );
        } elseif ($contentProperty instanceof AssocPattern) {
            $setValue = $this->builder->new(
                $this->useCollector->useClass(
                    AssocCollection::class
                ),
                [$setValue]
            );
        }

        $collection = $this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName());
        if ($pattern instanceof AssocPattern) {
            $key = $this->builder->var('key');
            $loopCall = new Expression($this->builder->methodCall(
                $collection,
                'offsetSet',
                [$key, $setValue]
            ));
            $initLoop = new Foreach_(
                $dataAccess,
                $item,
                [
                    'keyVar' => $key,
                    'stmts' => [$loopCall]
                ]
            );
        } else {
            $loopCall = new Expression($this->builder->methodCall(
                $collection,
                'add',
                [$setValue]
            ));
            $initLoop = new Foreach_(
                $dataAccess,
                $item,
                [
                    'stmts' => [$loopCall]
                ]
            );
        }
        $statements[] = $initLoop;

        return $statements;
    }
}