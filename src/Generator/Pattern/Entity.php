<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern;

use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use PhpParser\Node\Stmt\Namespace_;

class Entity extends AbstractPattern
{
    public function build(EntityPattern $pattern): Namespace_
    {
        $namespace = $this->builder->namespace(
            $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_ENTITIES
        );

        $class = $this->builder->class($pattern->getName());
        $class->extend($this->useCollector->useClass(
            $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES
            . '\\Generated' . $pattern->getName()
        ));

        $namespace->addStmts($this->useCollector->buildUseStatements());
        $namespace->addStmt($class);

        return $namespace->getNode();
    }
}