<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern;

use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use PhpParser\Node\Stmt\Namespace_;
use SimpleCollection\Entity\AbstractEntity;

class AbstractGeneratedEntity extends AbstractPattern
{
    const ABSTRACT_ENTITY_NAME = 'AbstractGeneratedEntity';


    public function build(): Namespace_
    {
        $namespace = $this->builder->namespace(
            $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES
        );

        $class = $this->builder->class(self::ABSTRACT_ENTITY_NAME);
        $class->makeAbstract();
        $class->extend($this->useCollector->useClass(AbstractEntity::class));
        $class->implement('\JsonSerializable');

        $construct = $this->builder->method('__construct');
        $construct->makePublic();
        $construct->addParam($this->builder->param('data')->setType('array')->setDefault([]));
        $construct->addStmt(
            $this->builder->methodCall(
                $this->builder->var('this'),
                'populate',
                [$this->builder->var('data')]
            )
        );
        $class->addStmt($construct);

        $populate = $this->builder->method('populate');
        $populate->makeAbstract();
        $populate->makePublic();
        $populate->addParam($this->builder->param('data')->setType('array'));
        $class->addStmt($populate);

        $namespace->addStmts($this->useCollector->buildUseStatements());
        $namespace->addStmt($class);

        return $namespace->getNode();
    }
}