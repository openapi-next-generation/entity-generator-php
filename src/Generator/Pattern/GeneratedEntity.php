<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern;

use OpenapiNextGeneration\EntityGeneratorPhp\Config\GenerationConfig;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity\Populate;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity\Property;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity\ToArray;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\UseCollector;
use OpenapiNextGeneration\EntityGeneratorPhp\Result\Collector;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use PhpParser\Node\Stmt\Namespace_;

class GeneratedEntity extends AbstractPattern
{
    protected Collector $collector;


    public function __construct(GenerationConfig $config, string $targetNamespace, Collector $collector)
    {
        parent::__construct(
            $config,
            $targetNamespace,
            new UseCollector($targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES)
        );
        $this->collector = $collector;
    }

    public function build(EntityPattern $pattern): Namespace_
    {
        $namespace = $this->builder->namespace(
            $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES
        );

        $class = $this->builder->class('Generated' . $pattern->getName());
        $class->makeAbstract();
        $class->extend($this->useCollector->useClass(
            $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES . '\\'
            . AbstractGeneratedEntity::ABSTRACT_ENTITY_NAME
        ));

        $properties = [];
        foreach ($pattern->getProperties() as $propertyPattern) {
            $properties[] = $property = new Property(
                $this->config,
                $this->targetNamespace,
                $this->useCollector,
                $this->collector,
                $propertyPattern
            );
            $class->addStmt($property->build());
        }
        $populate = new Populate($this->config, $this->targetNamespace, $this->useCollector);
        $class->addStmt($populate->build($properties));
        $toArray = new ToArray($this->config, $this->targetNamespace, $this->useCollector);
        $class->addStmt($toArray->build($properties));

        $namespace->addStmts($this->useCollector->buildUseStatements());
        $namespace->addStmt($class);

        return $namespace->getNode();
    }
}