<?php

use OpenapiNextGeneration\OpenapiResolverPhp\AllOfResolver;
use OpenapiNextGeneration\OpenapiResolverPhp\ReferenceResolver;

class EntityGeneratorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \OpenapiNextGeneration\EntityGeneratorPhp\Result\Collector
     */
    protected static $collector;


    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        $specificationFile = __DIR__ . '/api.yml';

        $referenceResolver = new ReferenceResolver();
        $specification = $referenceResolver->resolveReference($specificationFile)->getValue();

        $specification = $referenceResolver->resolveAllReferences(
            $specification,
            $specificationFile
        );

        $allOfResolver = new AllOfResolver();
        $specification = $allOfResolver->resolveKeywordAllOf($specification);

        $patternMapper = new \OpenapiNextGeneration\OpenapiPatternMapperPhp\PatternMapper();
        $patterns = $patternMapper->buildPatterns($specification);

        $entityGenerator = new \OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGenerator();
        $entityGenerator->processPatterns(
            $patterns,
            'Test'
        );
        self::$collector = $entityGenerator->getResultCollector();
    }

    public function testCreatedClassesExists()
    {
        $this->assertEquals(
            ['AbstractGeneratedEntity', 'GeneratedCountry', 'GeneratedItem', 'GeneratedTest'],
            array_keys(self::$collector->getGeneratedEntities())
        );

        $this->assertEquals(
            ['Country', 'Item', 'Test'],
            array_keys(self::$collector->getEntities())
        );

        $this->assertEquals(
            ['CitiesCollection', 'ItemCollection'],
            array_keys(self::$collector->getCollections())
        );
    }

    public function testAbstractGeneratedEntityContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\GeneratedEntities;

use SimpleCollection\Entity\AbstractEntity;
abstract class AbstractGeneratedEntity extends AbstractEntity implements \JsonSerializable
{
    public function __construct(array \$data = [])
    {
        \$this->populate(\$data);
    }
    public abstract function populate(array \$data);
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getGeneratedEntities()['AbstractGeneratedEntity']
        );
    }

    /**
     * @depends testCreatedClassesExists
     */
    public function testGeneratedCountryContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\GeneratedEntities;

abstract class GeneratedCountry extends AbstractGeneratedEntity
{
    public string \$name;
    public function populate(array \$data)
    {
        if (isset(\$data['name'])) {
            \$this->name = \$data['name'];
        }
    }
    public function toArray() : array
    {
        \$return = [];
        if (isset(\$this->name)) {
            \$return['name'] = \$this->name;
        }
        return \$return;
    }
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getGeneratedEntities()['GeneratedCountry']
        );
    }

    /**
     * @depends testCreatedClassesExists
     */
    public function testGeneratedItemContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\GeneratedEntities;

abstract class GeneratedItem extends AbstractGeneratedEntity
{
    public int \$age;
    public function populate(array \$data)
    {
        \$this->age = \$data['age'] ?? 2;
    }
    public function toArray() : array
    {
        return ['age' => \$this->age];
    }
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getGeneratedEntities()['GeneratedItem']
        );
    }

    /**
     * @depends testCreatedClassesExists
     */
    public function testGeneratedTestContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\GeneratedEntities;

use SimpleCollection\ArrayCollection;
use SimpleCollection\Entity\EntityArrayCollection;
use Test\Collections\ItemCollection;
use Test\Entities\Country;
use Test\Entities\Item;
abstract class GeneratedTest extends AbstractGeneratedEntity
{
    public string \$name;
    public string \$food;
    public ArrayCollection \$favoriteNumbers;
    public ArrayCollection \$cities;
    public Country \$country;
    public ?Item \$item;
    public EntityArrayCollection \$itemGroups;
    public ItemCollection \$itemGroup;
    public function populate(array \$data)
    {
        if (isset(\$data['name'])) {
            \$this->name = \$data['name'];
        }
        \$this->food = \$data['food'] ?? 'Cake';
        \$this->favoriteNumbers = new ArrayCollection();
        foreach (\$data['favorite-numbers'] ?? [2, 5] as \$item) {
            \$this->favoriteNumbers->add(\$item);
        }
        \$this->cities = new ArrayCollection();
        foreach (\$data['cities'] ?? [] as \$item) {
            \$this->cities->add(\$item);
        }
        \$this->country = new Country(\$data['country'] ?? []);
        \$this->item = isset(\$data['item']) ? new Item(\$data['item']) : null;
        \$this->itemGroups = new EntityArrayCollection();
        foreach (\$data['item-groups'] ?? [] as \$item) {
            \$this->itemGroups->add(new ArrayCollection(\$item));
        }
        \$this->itemGroup = new ItemCollection();
        foreach (\$data['item-group'] ?? [] as \$item) {
            \$this->itemGroup->add(new Item(\$item));
        }
    }
    public function toArray() : array
    {
        return ['name' => \$this->name, 'food' => \$this->food, 'favorite-numbers' => \$this->favoriteNumbers ? \$this->favoriteNumbers->getAll() : null, 'cities' => \$this->cities ? \$this->cities->getAll() : null, 'country' => \$this->country->toArray(), 'item' => \$this->item ? \$this->item->toArray() : null, 'item-groups' => \$this->itemGroups ? \$this->itemGroups->toArray() : null, 'item-group' => \$this->itemGroup ? \$this->itemGroup->toArray() : null];
    }
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getGeneratedEntities()['GeneratedTest']
        );
    }

    /**
     * @depends testCreatedClassesExists
     */
    public function testItemContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\Entities;

use Test\GeneratedEntities\GeneratedItem;
class Item extends GeneratedItem
{
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getEntities()['Item']
        );
    }

    /**
     * @depends testCreatedClassesExists
     */
    public function testItemCollectionContent()
    {
        $entity =
<<< ENTITY
<?php

namespace Test\Collections;

use SimpleCollection\Entity\EntityArrayCollection;
use Test\Entities\Item;
/**
 * @method Item current()
 * @method Item|false next()
 * @method Item scNext()
 * @method Item|false prev()
 * @method Item|false scPrev()
 * @method Item|false rewind()
 * @method Item|false end()
 * @method Item offsetGet(\$mOffset)
 * @method Item get(\$mOffset, \$mDefault = null)
 * @method Item seek(\$iOffset)
 * @method Item seekToKey(\$mKey, bool \$bStrictMode = true)
 */
class ItemCollection extends EntityArrayCollection
{
}
ENTITY;

        $this->assertEquals(
            $entity,
            self::$collector->getCollections()['ItemCollection']
        );
    }
}